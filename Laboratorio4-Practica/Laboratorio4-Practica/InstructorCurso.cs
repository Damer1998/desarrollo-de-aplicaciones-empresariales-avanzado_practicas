﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// Diseño
using MaterialSkin;

// Conexion
using System.Data.SqlClient;

using System.Configuration;

namespace Laboratorio4_Practica
{
    public partial class InstructorCurso : MaterialSkin.Controls.MaterialForm
    {
        public InstructorCurso()
        {
            InitializeComponent();
        }
        // Form Curso Instructor
        private void InstructorCurso_Load(object sender, EventArgs e)
        {

        }

        private void btnListarIU_Click(object sender, EventArgs e)
        {
            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {
                conexion.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM CourseInstructor", conexion))
                {


                    MessageBox.Show("Ingreso de Tabla Instructor Curso Exitoso");
                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoIU.DataSource = dt;
                    dgvListadoIU.Refresh();
                }



            }
        }

        private void btnBuscarIU_Click(object sender, EventArgs e)
        {
            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {

                conexion.Open();


                using (SqlCommand cmd = new SqlCommand())
                {
                    String CU = txtNombreIU.Text;
                    cmd.CommandText = "BuscarCourseInstructor";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conexion;


                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@PersonID";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Value = CU;

                    cmd.Parameters.Add(param);

                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoIU.DataSource = dt;
                    dgvListadoIU.Refresh();

                }

            }
        }
    }
}
