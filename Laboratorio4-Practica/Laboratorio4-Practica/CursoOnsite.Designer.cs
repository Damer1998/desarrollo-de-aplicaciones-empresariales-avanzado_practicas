﻿namespace Laboratorio4_Practica
{
    partial class CursoOnsite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvListadoCO = new System.Windows.Forms.DataGridView();
            this.btnBuscarCO = new System.Windows.Forms.Button();
            this.btnListarCO = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNombreCO = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoCO)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvListadoCO
            // 
            this.dgvListadoCO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListadoCO.Location = new System.Drawing.Point(48, 286);
            this.dgvListadoCO.Name = "dgvListadoCO";
            this.dgvListadoCO.RowHeadersWidth = 51;
            this.dgvListadoCO.RowTemplate.Height = 24;
            this.dgvListadoCO.Size = new System.Drawing.Size(776, 394);
            this.dgvListadoCO.TabIndex = 79;
            // 
            // btnBuscarCO
            // 
            this.btnBuscarCO.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnBuscarCO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarCO.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarCO.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnBuscarCO.Location = new System.Drawing.Point(464, 220);
            this.btnBuscarCO.Name = "btnBuscarCO";
            this.btnBuscarCO.Size = new System.Drawing.Size(360, 34);
            this.btnBuscarCO.TabIndex = 78;
            this.btnBuscarCO.Text = "Buscar";
            this.btnBuscarCO.UseVisualStyleBackColor = false;
            this.btnBuscarCO.Click += new System.EventHandler(this.btnBuscarCO_Click);
            // 
            // btnListarCO
            // 
            this.btnListarCO.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnListarCO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListarCO.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListarCO.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnListarCO.Location = new System.Drawing.Point(48, 220);
            this.btnListarCO.Name = "btnListarCO";
            this.btnListarCO.Size = new System.Drawing.Size(387, 34);
            this.btnListarCO.TabIndex = 77;
            this.btnListarCO.Text = "Listar";
            this.btnListarCO.UseVisualStyleBackColor = false;
            this.btnListarCO.Click += new System.EventHandler(this.btnListarCO_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtNombreCO);
            this.panel2.Location = new System.Drawing.Point(48, 120);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(776, 94);
            this.panel2.TabIndex = 76;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(14, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 19);
            this.label3.TabIndex = 10;
            this.label3.Text = "Busqueda";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.Red;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(776, 30);
            this.label6.TabIndex = 16;
            this.label6.Text = "Herramienta Rapida - Curso Onsite";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNombreCO
            // 
            this.txtNombreCO.Location = new System.Drawing.Point(111, 43);
            this.txtNombreCO.Name = "txtNombreCO";
            this.txtNombreCO.Size = new System.Drawing.Size(650, 22);
            this.txtNombreCO.TabIndex = 11;
            // 
            // CursoOnsite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 748);
            this.Controls.Add(this.dgvListadoCO);
            this.Controls.Add(this.btnBuscarCO);
            this.Controls.Add(this.btnListarCO);
            this.Controls.Add(this.panel2);
            this.Name = "CursoOnsite";
            this.Text = "CursoOnsite";
            this.Load += new System.EventHandler(this.CursoOnsite_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoCO)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvListadoCO;
        private System.Windows.Forms.Button btnBuscarCO;
        private System.Windows.Forms.Button btnListarCO;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNombreCO;
    }
}