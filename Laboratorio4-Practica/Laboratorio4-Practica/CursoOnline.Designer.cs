﻿namespace Laboratorio4_Practica
{
    partial class CursoOnline
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvListadoCOL = new System.Windows.Forms.DataGridView();
            this.btnBuscarCOL = new System.Windows.Forms.Button();
            this.btnListarCOL = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNombreCOL = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoCOL)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvListadoCOL
            // 
            this.dgvListadoCOL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListadoCOL.Location = new System.Drawing.Point(49, 292);
            this.dgvListadoCOL.Name = "dgvListadoCOL";
            this.dgvListadoCOL.RowHeadersWidth = 51;
            this.dgvListadoCOL.RowTemplate.Height = 24;
            this.dgvListadoCOL.Size = new System.Drawing.Size(776, 394);
            this.dgvListadoCOL.TabIndex = 75;
            // 
            // btnBuscarCOL
            // 
            this.btnBuscarCOL.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnBuscarCOL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarCOL.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarCOL.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnBuscarCOL.Location = new System.Drawing.Point(465, 226);
            this.btnBuscarCOL.Name = "btnBuscarCOL";
            this.btnBuscarCOL.Size = new System.Drawing.Size(360, 34);
            this.btnBuscarCOL.TabIndex = 74;
            this.btnBuscarCOL.Text = "Buscar";
            this.btnBuscarCOL.UseVisualStyleBackColor = false;
            this.btnBuscarCOL.Click += new System.EventHandler(this.btnBuscarCOL_Click);
            // 
            // btnListarCOL
            // 
            this.btnListarCOL.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnListarCOL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListarCOL.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListarCOL.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnListarCOL.Location = new System.Drawing.Point(49, 226);
            this.btnListarCOL.Name = "btnListarCOL";
            this.btnListarCOL.Size = new System.Drawing.Size(387, 34);
            this.btnListarCOL.TabIndex = 73;
            this.btnListarCOL.Text = "Listar";
            this.btnListarCOL.UseVisualStyleBackColor = false;
            this.btnListarCOL.Click += new System.EventHandler(this.btnListarCOL_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtNombreCOL);
            this.panel2.Location = new System.Drawing.Point(49, 126);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(776, 94);
            this.panel2.TabIndex = 72;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(14, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 19);
            this.label3.TabIndex = 10;
            this.label3.Text = "Busqueda";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.Red;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(776, 30);
            this.label6.TabIndex = 16;
            this.label6.Text = "Herramienta Rapida - Curso Online";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtNombreCOL
            // 
            this.txtNombreCOL.Location = new System.Drawing.Point(111, 43);
            this.txtNombreCOL.Name = "txtNombreCOL";
            this.txtNombreCOL.Size = new System.Drawing.Size(650, 22);
            this.txtNombreCOL.TabIndex = 11;
            // 
            // CursoOnline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 748);
            this.Controls.Add(this.dgvListadoCOL);
            this.Controls.Add(this.btnBuscarCOL);
            this.Controls.Add(this.btnListarCOL);
            this.Controls.Add(this.panel2);
            this.Name = "CursoOnline";
            this.Text = "CursoOnline";
            this.Load += new System.EventHandler(this.CursoOnline_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoCOL)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvListadoCOL;
        private System.Windows.Forms.Button btnBuscarCOL;
        private System.Windows.Forms.Button btnListarCOL;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNombreCOL;
    }
}