﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// Diseño
using MaterialSkin;

// Conexion
using System.Data.SqlClient;

using System.Configuration;
namespace Laboratorio4_Practica
{
    public partial class Persona : MaterialSkin.Controls.MaterialForm
    {
        public Persona()
        {
            InitializeComponent();
        }

        // Form Persona
        private void Persona_Load(object sender, EventArgs e)
        {

        }

        private void btnListarP_Click(object sender, EventArgs e)
        {
            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {
                conexion.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Person", conexion))
                {


                    MessageBox.Show("Ingreso de Tabla Persona Exitoso");
                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoP.DataSource = dt;
                    dgvListadoP.Refresh();
                }



            }
        }

        private void btnBuscarP_Click(object sender, EventArgs e)
        {
            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {

                conexion.Open();


                using (SqlCommand cmd = new SqlCommand())
                {
                    String Nombre = txtNombreP.Text;
                    cmd.CommandText = "BuscarPersonaNombre";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conexion;


                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@FirstName";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Value = Nombre;

                    cmd.Parameters.Add(param);

                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoP.DataSource = dt;
                    dgvListadoP.Refresh();

                }

            }
        }
    }
}
