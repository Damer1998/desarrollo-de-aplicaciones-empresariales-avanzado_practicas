﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// Agregar referencia


// Diseño
using MaterialSkin;  // Instalar
using System.Configuration;

using System.Data.SqlClient;
namespace Laboratorio4_Practica
{
    public partial class PrincipalMDI : MaterialSkin.Controls.MaterialForm
    {




        public PrincipalMDI()
        {
            InitializeComponent();
        }

        // Void para CerrarlaForma
        void CerrarForma(object sender , FormClosedEventArgs e)
        {
            ingresoCC = null;
        }



        // Area -Salida Completo del Programa 
        private void SalirMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Area de - Cerrar Sesion
        private void CerrarSesionMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        // Crear Valores 
        CursoCompleto ingresoCC;
        // Are Curso  - Curso Completo
        private void CursosCompletoMenuItem_Click(object sender, EventArgs e)
        {
            if(ingresoCC == null)
            {
                ingresoCC = new CursoCompleto();
                ingresoCC.MdiParent = this;
                ingresoCC.FormClosed += new FormClosedEventHandler(CerrarForma);
                ingresoCC.Show();

            }
            else
            {
                ingresoCC.Activate();
            }
        }

        // Are Curso -  Cursos Online
        private void CursosOnlineMenuItem_Click(object sender, EventArgs e)
        {
            CursoOnline ingresoCO = new CursoOnline();
            ingresoCO.MdiParent = this;
            ingresoCO.Show();
        }

        // Area Curso - Curso Onsite
        private void CursosOnsiteMenuItem_Click(object sender, EventArgs e)
        {
            CursoOnsite ingresoCOE = new CursoOnsite();
            ingresoCOE.MdiParent = this;
            ingresoCOE.Show();
        }

        // Area Curso - Grado del Curso
        private void CursosGradoMenuItem_Click(object sender, EventArgs e)
        {
            GradoCurso ingresoGC = new GradoCurso();
            ingresoGC.MdiParent = this;
            ingresoGC.Show();
        }

        // Area de Curso - Instructor del Curso
        private void CursosInstructorMenuItem_Click(object sender, EventArgs e)
        {
            InstructorCurso ingresoCI = new InstructorCurso();
            ingresoCI.MdiParent = this;
            ingresoCI.Show();
        }


        // Crear Valores 
        AsignacionOficina ingresoAO;
        // Area Dato - Asignacion de Oficina
        private void AsignacionOficinaMenuItem_Click(object sender, EventArgs e)
        {
            if (ingresoAO == null)
            {
                ingresoAO = new AsignacionOficina();
                ingresoAO.MdiParent = this;
                ingresoAO.FormClosed += new FormClosedEventHandler(CerrarForma);
                ingresoAO.Show();

            }
            else
            {
                ingresoAO.Activate();
            }
        }

        // Area Dato - Persona
        private void PersonaMenuItem_Click(object sender, EventArgs e)
        {
            Persona ingresoAO = new Persona();
            ingresoAO.MdiParent = this;
            ingresoAO.Show();
        }

        // Area dato - Departamento
        private void DepartamentoMenuItem_Click(object sender, EventArgs e)
        {
            Departamento ingresoAO = new Departamento();
            ingresoAO.MdiParent = this;
            ingresoAO.Show();
        }


        // Prncipal MDI
        private void PrincipalMDI_Load(object sender, EventArgs e)
        {

        }

        private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }


        /*
         * Ingresaro CONSULTAS AL MYSQLServer
create proc BuscarPersonaNombre
@FirstName nvarchar(50)
as
select * from Person where FirstName like '%' + @FirstName + '%';


create proc BuscarCursoCompleto
@Title nvarchar(50)
as
select * from Course where Title like '%' + @Title + '%';

exec BuscarCursoCompleto Ca;


create proc BuscarCourseID
@CourseID nvarchar(50)
as
select * from OnsiteCourse where CourseID like '%' + @CourseID + '%';

exec BuscarCourseID 1045;


    create proc BuscarCourseIDLine
@CourseID nvarchar(50)
as
select * from OnlineCourse where CourseID like '%' + @CourseID + '%';

exec BuscarCourseIDLine 1045;



        create proc BuscarName
@Name nvarchar(50)
as
select * from Department where Name like '%' + @Name + '%';

exec BuscarName E;

create proc BuscarInstructorAsignado
@InstructorID nvarchar(50)
as
select InstructorID, Location from OfficeAssignment where InstructorID like '%' + @InstructorID + '%';

exec BuscarInstructorAsignado E;


create proc BuscarCourseInstructor
@PersonID nvarchar(50)
as
select * from CourseInstructor where PersonID like '%' + @PersonID + '%';

exec BuscarCourseInstructor 2;


    create proc BuscarCourseGrade
@EnrollmentID nvarchar(50)
as
select * from CourseGrade where EnrollmentID like '%' + @EnrollmentID + '%';

exec BuscarCourseGrade 2;

*/




    }
}
