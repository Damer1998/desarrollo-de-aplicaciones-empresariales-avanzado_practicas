﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Diseño
using MaterialSkin;

// Conexion
using System.Data.SqlClient;

using System.Configuration;


namespace Laboratorio4_Practica
{
    public partial class CursoCompleto : MaterialSkin.Controls.MaterialForm
    {





        public CursoCompleto()
        {
            InitializeComponent();

           
        }

        // Form Curso Completo
        private void CursoCompleto_Load(object sender, EventArgs e)
        {

        }

        // Boton Listar btnListarCC
        private void btnListarCC_Click(object sender, EventArgs e)
        {

            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {
                conexion.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT * FROM Course", conexion))
                {


                    MessageBox.Show("Ingreso de Tabla Curso Exitoso");
                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoCC.DataSource = dt;
                    dgvListadoCC.Refresh();
                }



            }





        }

        // Botono Buscar btnBuscarCC
        private void btnBuscarCC_Click(object sender, EventArgs e)
        {

            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {

                conexion.Open();


                using (SqlCommand cmd = new SqlCommand())
                {
                    String Title = txtNombreCC.Text;
                    cmd.CommandText = "BuscarCursoCompleto";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conexion;


                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@Title";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Value = Title;

                    cmd.Parameters.Add(param);

                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoCC.DataSource = dt;
                    dgvListadoCC.Refresh();

                }

            }


        }
    }
}
