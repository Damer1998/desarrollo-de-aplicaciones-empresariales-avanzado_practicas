﻿namespace Laboratorio4_Practica
{
    partial class AsignacionOficina
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvListadoAO = new System.Windows.Forms.DataGridView();
            this.btnBuscarAO = new System.Windows.Forms.Button();
            this.btnListarAO = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textNombreAO = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoAO)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvListadoAO
            // 
            this.dgvListadoAO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListadoAO.Location = new System.Drawing.Point(46, 301);
            this.dgvListadoAO.Name = "dgvListadoAO";
            this.dgvListadoAO.RowHeadersWidth = 51;
            this.dgvListadoAO.RowTemplate.Height = 24;
            this.dgvListadoAO.Size = new System.Drawing.Size(776, 394);
            this.dgvListadoAO.TabIndex = 95;
            // 
            // btnBuscarAO
            // 
            this.btnBuscarAO.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnBuscarAO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarAO.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarAO.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnBuscarAO.Location = new System.Drawing.Point(462, 235);
            this.btnBuscarAO.Name = "btnBuscarAO";
            this.btnBuscarAO.Size = new System.Drawing.Size(360, 34);
            this.btnBuscarAO.TabIndex = 94;
            this.btnBuscarAO.Text = "Buscar";
            this.btnBuscarAO.UseVisualStyleBackColor = false;
            this.btnBuscarAO.Click += new System.EventHandler(this.btnBuscarAO_Click);
            // 
            // btnListarAO
            // 
            this.btnListarAO.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnListarAO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListarAO.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListarAO.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnListarAO.Location = new System.Drawing.Point(46, 235);
            this.btnListarAO.Name = "btnListarAO";
            this.btnListarAO.Size = new System.Drawing.Size(387, 34);
            this.btnListarAO.TabIndex = 93;
            this.btnListarAO.Text = "Listar";
            this.btnListarAO.UseVisualStyleBackColor = false;
            this.btnListarAO.Click += new System.EventHandler(this.btnListarAO_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.textNombreAO);
            this.panel2.Location = new System.Drawing.Point(46, 135);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(776, 94);
            this.panel2.TabIndex = 92;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(14, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 19);
            this.label3.TabIndex = 10;
            this.label3.Text = "Busqueda";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.Red;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(776, 30);
            this.label6.TabIndex = 16;
            this.label6.Text = "Herramienta Rapida -Asignacion Oficio";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textNombreAO
            // 
            this.textNombreAO.Location = new System.Drawing.Point(111, 43);
            this.textNombreAO.Name = "textNombreAO";
            this.textNombreAO.Size = new System.Drawing.Size(650, 22);
            this.textNombreAO.TabIndex = 11;
            // 
            // AsignacionOficina
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 748);
            this.Controls.Add(this.dgvListadoAO);
            this.Controls.Add(this.btnBuscarAO);
            this.Controls.Add(this.btnListarAO);
            this.Controls.Add(this.panel2);
            this.Name = "AsignacionOficina";
            this.Text = "AsignacionOficina";
            this.Load += new System.EventHandler(this.AsignacionOficina_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListadoAO)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvListadoAO;
        private System.Windows.Forms.Button btnBuscarAO;
        private System.Windows.Forms.Button btnListarAO;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textNombreAO;
    }
}