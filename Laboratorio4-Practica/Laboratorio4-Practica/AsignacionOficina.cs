﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


// Diseño
using MaterialSkin;

// Conexion
using System.Data.SqlClient;

using System.Configuration;



namespace Laboratorio4_Practica
{
    public partial class AsignacionOficina : MaterialSkin.Controls.MaterialForm
    {
        public AsignacionOficina()
        {
            InitializeComponent();
        }

        private void AsignacionOficina_Load(object sender, EventArgs e)
        {

        }

        private void btnListarAO_Click(object sender, EventArgs e)
        {
            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {
                conexion.Open();

                using (SqlCommand cmd = new SqlCommand("SELECT InstructorID, Location FROM OfficeAssignment", conexion))
                {


                    MessageBox.Show("Ingreso de Tabla Asignacion Oficio Exitoso");
                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoAO.DataSource = dt;
                    dgvListadoAO.Refresh();
                }



            }
        }

        private void btnBuscarAO_Click(object sender, EventArgs e)
        {
            string cnn = ConfigurationManager.ConnectionStrings["cnn"].ConnectionString;
            using (SqlConnection conexion = new SqlConnection(cnn))
            {

                conexion.Open();


                using (SqlCommand cmd = new SqlCommand())
                {
                    String Title = textNombreAO.Text;
                    cmd.CommandText = "BuscarInstructorAsignado";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = conexion;


                    SqlParameter param = new SqlParameter();
                    param.ParameterName = "@InstructorID";
                    param.SqlDbType = SqlDbType.NVarChar;
                    param.Value = Title;

                    cmd.Parameters.Add(param);

                    SqlDataReader reader = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    dgvListadoAO.DataSource = dt;
                    dgvListadoAO.Refresh();

                }

            }
        }
    }
}
